/*
 * Use inline (highlight) coding to get type and source
 * of the diagram. Based on GitLab markdown rendering
 */
function renderInlineKrokiDiagram(inline) {
    var firstLine = inline.split('\n')[1];
    var diagramType = firstLine.split('```')[1];
    var diagramSource = inline.split(firstLine)[1].split('```')[0];
    renderKrokiDiagram(diagramType, diagramSource);    
}

/*
 * Based on https://docs.kroki.io/kroki/setup/encode-diagram/#javascript
 */
function renderKrokiDiagram(diagramType, diagramSource) {
    var data = textEncode(diagramSource);
    var compressed = pako.deflate(data, { level: 9, to: 'string' });
    var krokiParam = btoa(compressed).replace(/\+/g, '-').replace(/\//g, '_');
    var krokiUrl = 'https://kroki.io/' + diagramType + '/svg/' + krokiParam;
    document.write('<figure>');
    document.write('<img src="' + krokiUrl + '"/>');
    document.write('</figure>');
}

function textEncode(str) {
    if (window.TextEncoder) {
        return new TextEncoder('utf-8').encode(str);
    }
    var utf8 = unescape(encodeURIComponent(str));
    var result = new Uint8Array(utf8.length);
    for (var i = 0; i < utf8.length; i++) {
        result[i] = utf8.charCodeAt(i);
    }
    return result;
}
